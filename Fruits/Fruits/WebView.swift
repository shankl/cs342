//
//  WebView.swift
//  Fruits
//
//  Created by Liz Shank on 4/18/15.
//
import UIKit
class WebView: UIViewController {
    @IBOutlet var myWebView: UIWebView!

    var favorites = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        var urlString = "http://allrecipes.com/search/default.aspx?"
        
        // search for recipes using favorite fruits & vegetables
        // we could not get this to work; we think it's because of an issue using the mobile website
        /*
        for (index,fav) in enumerate(self.favorites) {
            urlString = urlString + "&w"
            urlString = urlString + String(index)
            urlString = urlString + "="
            urlString = urlString + fav
        }
        println(urlString)
        */
        let url = NSURL(string: urlString)
        let requestObj = NSURLRequest(URL: url!)
        myWebView.loadRequest(requestObj)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}