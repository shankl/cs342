//
//  TableViewController.swift
//  ListDemo
//
//  Created by Liz Shank on 4/15/15.
//  Copyright (c) 2015 Jeff Ondich. All rights reserved.
//  Modified by Liz Shank and Max Willard 4/20/15
//

import UIKit
protocol FruitsDelegate {
    func favoritesDidChange(name: [String])
}

class FruitsInSeason: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var fruitOptionsTableView: UITableView?
    
    var favorites = [String]()
    var delegate: FruitsDelegate?
    
    // Seasonal fruits and vegetable lists
    
    let spring = ["Apricots"
        , "Artichoke"
        , "Broccoli"
        , "Corn"
        , "Honeydew"
        , "Mango"
        , "Oranges"
        , "Pineapple"
        , "Strawberries"]
    
    let summer = ["Beets",
        "Bell Peppers",
        "Blueberries",
        "Cantaloupe",
        "Cherries",
        "Cucumbers",
        "Figs",
        "Grapefruit",
        "Grapes",
        "Passion Fruit",
        "Peaches",
        "Plums",
        "Raspberries",
        "Tomatoes",
        "Watermelons"]
    
    let fall = ["Acorn Squash",
        "Cauliflower",
        "Cranberries",
        "Kumquats",
        "Mushrooms",
        "Pomegranate",
        "Pumpkin",
        "Sweet Potatoes",
        "Turnips"]
    
    let winter = ["Pears",
        "Pomegranate",
        "Kale",
        "Kiwifruit",
        "Tangerines"]
    
    let cellIdentifier = "FruitsCell"
    var currentSeason = "holder"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.fruitOptionsTableView?.delegate = self
        self.fruitOptionsTableView?.dataSource = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - UITableViewDataSource
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Determine the current month
        let date = NSDate()
        let calendar = NSCalendar.currentCalendar()
        let components = calendar.components(.CalendarUnitMonth, fromDate: date)
        let month = components.month
        
        // Determine the current season and return the number of values
        // in the corresponding fruits/vegetables list
        if (month >= 3 && month <= 5) {
            self.currentSeason = "Spring"
            return self.spring.count
        }
            
        else if (month >= 6 && month <= 8) {
            self.currentSeason = "Summer"
            return self.summer.count

        }
            
        else if (month >= 9 && month <= 11) {
            self.currentSeason = "Fall"
            return self.fall.count

        }
            
        else {
            self.currentSeason = "Winter"
            return self.winter.count

        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: UITableViewCell! = tableView.dequeueReusableCellWithIdentifier(self.cellIdentifier) as? UITableViewCell
        if cell == nil {
            cell = UITableViewCell(style: .Default, reuseIdentifier: self.cellIdentifier)
            cell.selectionStyle = .None
            cell.accessoryType = UITableViewCellAccessoryType.None
        }
        // Determine the current month
        let date = NSDate()
        let calendar = NSCalendar.currentCalendar()
        let components = calendar.components(.CalendarUnitMonth, fromDate: date)
        let month = components.month
        
        // Determine the current season and display values from the corresponding
        // fruits/vegetables list
        if (month >= 3 && month <= 5) {
            cell.textLabel?.text = self.spring[indexPath.row]
        }
        
        else if (month >= 6 && month <= 8) {
            cell.textLabel?.text = self.summer[indexPath.row]
        }
        
        else if (month >= 9 && month <= 11) {
            cell.textLabel?.text = self.fall[indexPath.row]
        }
        
        else {
            cell.textLabel?.text = self.winter[indexPath.row]
        }
        
        if let fruit = cell!.textLabel!.text {
            let selectedFruit = cell!.textLabel!.text!
            
            // For all fruits/vegetables saved in the user's favorites list,
            // add a checkmark to their cell
            for (index,fav) in enumerate(self.favorites) {
                if fav == fruit {
                    cell?.accessoryType = UITableViewCellAccessoryType.Checkmark
                }
            }
        }

        return cell
    }
    
    // MARK: - UITableViewDelegate
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(tableView: UITableView, willDeselectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
        var cell: UITableViewCell? = tableView.cellForRowAtIndexPath(indexPath)
        return indexPath
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var cell: UITableViewCell? = tableView.cellForRowAtIndexPath(indexPath)
        if let fruit = cell!.textLabel!.text {
            var inFavs = false
            let selectedFruit = cell!.textLabel!.text!
            
            for (index,fav) in enumerate(self.favorites) {
                if fav == selectedFruit {
                    self.favorites.removeAtIndex(index)
                    inFavs = true
                    // remove checkmark
                    cell?.accessoryType = UITableViewCellAccessoryType.None
                }
            }
            if inFavs == false {
                self.favorites.append(cell!.textLabel!.text!)
                // add checkmark
                cell?.accessoryType = UITableViewCellAccessoryType.Checkmark
            }
        }
    }
    
    override func didMoveToParentViewController(parent: UIViewController?) {
        if parent == nil {
            // parent is nil when the FruitsInSeason Controller is removed from the UINavigationController
            // For this program, that's exactly when we want to alert the main ViewController
            // of changes to the favorites list
            
            self.delegate?.favoritesDidChange(self.favorites)
        }
    }

}

