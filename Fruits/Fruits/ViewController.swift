//
//  ViewController.swift
//  ListDemo
//
//  Created by Jeffrey Ondich on 4/9/15.
//  Copyright (c) 2015 Jeff Ondich. All rights reserved.
//  Modified by Liz Shank and Max Willard 4/20/15
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, FruitsDelegate{
    @IBOutlet weak var fruitOptionsTableView: UITableView?
    
    var favorites = [String]()

    let cellIdentifier = "FruitCell"
    let options = ["Seasonal Fruits & Vegetables", "Favorites", "Search for Recipes"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.fruitOptionsTableView?.delegate = self
        self.fruitOptionsTableView?.dataSource = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - UITableViewDataSource
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.options.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: UITableViewCell! = tableView.dequeueReusableCellWithIdentifier(self.cellIdentifier) as? UITableViewCell
        if cell == nil {
            cell = UITableViewCell(style: .Default, reuseIdentifier: self.cellIdentifier)
            cell.selectionStyle = .None
            cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator;
        }
        
        cell.textLabel?.text = self.options[indexPath.row]
        
        return cell
    }
    
    // MARK: - UITableViewDelegate
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 40
    }

    func tableView(tableView: UITableView, willDeselectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
        var cell: UITableViewCell? = tableView.cellForRowAtIndexPath(indexPath)
        return indexPath
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if (indexPath.row == 0) {
            performSegueWithIdentifier("GoToFruits", sender: self)
        }
        else if (indexPath.row == 1) {
            performSegueWithIdentifier("GoToFavorites", sender: self)
        }
        else if (indexPath.row == 2) {
            performSegueWithIdentifier("GoToWebView", sender: self)
        }

        var cell: UITableViewCell? = tableView.cellForRowAtIndexPath(indexPath)
        cell?.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
    }
    
    // We handle the Cow button, which is hooked to the CowViewController in
    // Main.storyboard, via the segue that we set up in the storyboard. We're sending
    // information to the CowViewController (i.e. the current cow name) via the
    // CowViewController's own cowName instance variable. We also
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier != nil && segue.identifier! == "GoToFruits" {
            var fruitsVC = segue.destinationViewController as! FruitsInSeason
            
            // Send information (i.e. the current value of the favorites list) to the
            // FruitsInSeason Controller via its own favorites instance variable.
            fruitsVC.favorites = self.favorites
            
            // We're going to receive information back from the FruitsInSeason Controller
            // via the FruitsDelegate protocol method. So we assign this ViewController
            // to be the FruitsInSeason Controller's delegate. That way, when the time comes,
            // our own favoritesDidChange method will get called by FruitsInSeason Controller.
            fruitsVC.delegate = self
        }
        else if segue.identifier != nil && segue.identifier! == "GoToFavorites" {
            var favoritesVC = segue.destinationViewController as! Favorites
            // Send information (i.e. the current value of the favorites list) to the
            // Favorites Controller via its own favorites instance variable.
            favoritesVC.favorites = self.favorites

        }
        else if segue.identifier != nil && segue.identifier! == "GoToWebView" {
            var webVC = segue.destinationViewController as! WebView
            // Send information (i.e. the current value of the favorites list) to the
            // WebView Controller via its own favorites instance variable.
            webVC.favorites = self.favorites
        }
    }
    // MARK: - FruitsDelegate
    func favoritesDidChange(name: [String]) {
        self.favorites = name
    }

}